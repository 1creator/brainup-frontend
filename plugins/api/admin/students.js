export default {
    route: "students",
    async fetch(params) {
        return (await this.$axios.get(`/admin/${this.route}`, { params }));
    },
    async fetchOne(id, params) {
        return (await this.$axios.get(`/admin/${this.route}/${id}`, { params }));
    },
    async fetchStudentAttestations(id, params) {
        return await this.$axios.get(`/admin/${this.route}/${id}/attestations`, { params });
    },
    async fetchStudentApps(id, params) {
        return (await this.$axios.get(`/admin/${this.route}/${id}/apps`, { params }));
    },
    async store(params) {
        params = JSON.parse(JSON.stringify(params));
        if (params.group) {
            params.groupId = params.group.id;
            delete params.group;
        }
        return (await this.$axios.post(`/admin/${this.route}`, params));
    },
    async storeSubscription(studentId, params) {
        params = JSON.parse(JSON.stringify(params));
        return (await this.$axios.post(`/admin/${this.route}/${studentId}/subscriptions`, params));
    },
    async update(id, params) {
        params = JSON.parse(JSON.stringify(params));
        if (params.group) {
            params.groupId = params.group.id;
            delete params.group;
        }
        return (await this.$axios.put(`/admin/${this.route}/${id}`, params));
    },
    async delete(id, params) {
        return (await this.$axios.delete(`/admin/${this.route}/${id}`, params));
    },
};
