export default {
    route: "payments",
    async fetch(params) {
        return (await this.$axios.get(`/admin/${this.route}`, { params }));
    },
    async fetchOne(id, params) {
        return (await this.$axios.get(`/admin/${this.route}/${id}`, { params }));
    },
    async store(params) {
        params = JSON.parse(JSON.stringify(params));
        return (await this.$axios.post(`/admin/${this.route}`, params));
    },
    async update(id, params) {
        params = JSON.parse(JSON.stringify(params));
        return (await this.$axios.put(`/admin/${this.route}/${id}`, params));
    },
    async delete(id, params) {
        return (await this.$axios.delete(`/admin/${this.route}/${id}`, params));
    },
};
