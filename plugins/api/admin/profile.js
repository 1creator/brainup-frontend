export default {
    async update(params) {
        params = JSON.parse(JSON.stringify(params));
        if (params.image !== undefined) {
            params.imageId = params.image && params.image.id;
            delete params.image;
        }
        return await this.$axios.put("admin/profile", params);
    },
};
