export default {
    route: "subscriptions",
    async delete(id, params) {
        return (await this.$axios.delete(`/admin/${this.route}/${id}`, params));
    },
};
