export default {
    route: "apps",
    async fetch(params) {
        return await this.$axios.get(`/admin/${this.route}`, { params });
    },
    async fetchStudents(id, params) {
        return await this.$axios.get(`/admin/${this.route}/${id}/students`, {
            params,
        });
    },
    async fetchSessions(id, params) {
        return await this.$axios.get(`/admin/${this.route}/${id}/sessions`, {
            params,
        });
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`/admin/${this.route}/${id}`, { params });
    },
    async play(id, params) {
        return await this.$axios.get(`/admin/${this.route}/${id}/play`, { params });
    },
    async store(params) {
        params = JSON.parse(JSON.stringify(params));
        if (params.students) {
            params.studentIds = params.students.map(item => item.id);
            delete params.students;
        }
        return await this.$axios.post(`/admin/${this.route}`, params);
    },
    async update(id, params) {
        params = JSON.parse(JSON.stringify(params));
        if (params.students) {
            params.studentIds = params.students.map(item => item.id);
            delete params.students;
        }
        return await this.$axios.put(`/admin/${this.route}/${id}`, params);
    },
    async delete(id, params) {
        return await this.$axios.delete(`/admin/${this.route}/${id}`, params);
    },
};
