export default {
    route: "teachers",
    async fetch(params) {
        return (await this.$axios.get(`/admin/${this.route}`, { params }));
    },
    async fetchOne(id, params) {
        return (await this.$axios.get(`/admin/${this.route}/${id}`, { params }));
    },
    async fetchTeacherApps(id, params) {
        return (await this.$axios.get(`/admin/${this.route}/${id}/apps`, { params }));
    },
    async store(params) {
        params = JSON.parse(JSON.stringify(params));
        return (await this.$axios.post(`/admin/${this.route}`, params));
    },
    async storeSubscription(teacherId, params) {
        params = JSON.parse(JSON.stringify(params));
        return (await this.$axios.post(`/admin/${this.route}/${teacherId}/subscriptions`, params));
    },
    async update(id, params) {
        params = JSON.parse(JSON.stringify(params));
        return (await this.$axios.put(`/admin/${this.route}/${id}`, params));
    },
    async delete(id, params) {
        return (await this.$axios.delete(`/admin/${this.route}/${id}`, params));
    },
};
