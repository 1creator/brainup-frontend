export default {
    route: "attestations",
    async fetch(params) {
        return await this.$axios.get(`/admin/${this.route}`, { params });
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`/admin/${this.route}/${id}`, { params });
    },
    async delete(id, params) {
        return await this.$axios.delete(`/admin/${this.route}/${id}`, { params });
    },
    async deleteLevel(id) {
        return await this.$axios.delete(`/admin/attestation-levels/${id}`);
    },
    async fetchStage(stageId, params) {
        return await this.$axios.get(`/admin/attestation-stages/${stageId}`, { params });
    },
};
