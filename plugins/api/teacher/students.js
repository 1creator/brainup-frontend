export default {
    route: "students",
    async fetch(params) {
        return await this.$axios.get(`/teacher/${this.route}`, { params });
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`/teacher/${this.route}/${id}`, { params });
    },
    async fetchStudentAttestations(id, params) {
        return await this.$axios.get(`/teacher/${this.route}/${id}/attestations`, { params });
    },
    async fetchStudentApps(id, params) {
        return await this.$axios.get(`/teacher/${this.route}/${id}/apps`, { params });
    },
    async grantAttestationLevel(studentId, params) {
        return await this.$axios.post(`/teacher/${this.route}/${studentId}/attestation-level`, params);
    },
    async fetchStudentSessions(id, params) {
        return await this.$axios.get(`/teacher/${this.route}/${id}/sessions`, {
            params,
        });
    },
    async fetchStudentBrainsHistory(id, params) {
        return await this.$axios.get(`/teacher/${this.route}/${id}/brains`, { params });
    },
    async store(params) {
        params = JSON.parse(JSON.stringify(params));
        if (params.group) {
            params.groupId = params.group.id;
            delete params.group;
        }
        return await this.$axios.post(`/teacher/${this.route}`, params);
    },
    async invite(params) {
        params = JSON.parse(JSON.stringify(params));
        return await this.$axios.post(`/teacher/${this.route}/invite`, params);
    },
    async update(id, params) {
        params = JSON.parse(JSON.stringify(params));
        if (params.group) {
            params.groupId = params.group.id;
            delete params.group;
        }
        return await this.$axios.put(`/teacher/${this.route}/${id}`, params);
    },
    async delete(id, params) {
        return await this.$axios.delete(`/teacher/${this.route}/${id}`, params);
    },
};
