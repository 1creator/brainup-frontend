export default {
    route: "subscriptions",
    async give(params) {
        params = JSON.parse(JSON.stringify(params));
        return await this.$axios.post(`/teacher/${this.route}/give`, params);
    },
    async fetchTransactions(params) {
        // params = JSON.parse(JSON.stringify(params));
        return await this.$axios.get(`/teacher/${this.route}/transactions`, { params });
    },
    async buyPackage(params) {
        // params = JSON.parse(JSON.stringify(params));
        return await this.$axios.get(`/teacher/${this.route}/buy-package`, { params });
    },
    async buySubscription(params) {
        // params = JSON.parse(JSON.stringify(params));
        return await this.$axios.get(`/teacher/${this.route}/buy-subscription`, {
            params,
        });
    },
};
