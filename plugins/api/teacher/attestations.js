export default {
    route: "attestations",
    async fetch(params) {
        return await this.$axios.get(`/teacher/${this.route}`, { params });
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`/teacher/${this.route}/${id}`, { params });
    },
    async delete(id, params) {
        return await this.$axios.delete(`/teacher/${this.route}/${id}`, { params });
    },
    async store(params) {
        return await this.$axios.post(`/teacher/${this.route}`, params);
    },
    async assign(params) {
        params = JSON.parse(JSON.stringify(params));
        if (params.students) {
            params.studentIds = params.students.map(item => item.id);
            delete params.students;
        }
        return await this.$axios.post(`/teacher/${this.route}/assign`, params);
    },
    async generateVariants(params) {
        return await this.$axios.post(`/teacher/${this.route}/generate-variants`, params);
    },
    async fetchStage(stageId, params) {
        return await this.$axios.get(`/teacher/attestation-stages/${stageId}`, { params });
    },
    async answerStage(stageId, params) {
        return await this.$axios.post(`/teacher/attestation-stages/${stageId}/answer`, params);
    },
};
