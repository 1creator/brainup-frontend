export default {
    route: "apps",
    async fetch(params) {
        return await this.$axios.get(`/teacher/${this.route}`, { params });
    },
    async fetchStudents(id, params) {
        return await this.$axios.get(`/teacher/${this.route}/${id}/students`, {
            params,
        });
    },
    async fetchSessions(id, params) {
        return await this.$axios.get(`/teacher/${this.route}/${id}/sessions`, {
            params,
        });
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`/teacher/${this.route}/${id}`, { params });
    },
    async play(id, params) {
        return await this.$axios.get(`/teacher/${this.route}/${id}/play`, { params });
    },
    async store(params) {
        params = JSON.parse(JSON.stringify(params));
        if (params.students) {
            params.studentIds = params.students.map(item => item.id);
            delete params.students;
        }
        return await this.$axios.post(`/teacher/${this.route}`, params);
    },
    async update(id, params) {
        params = JSON.parse(JSON.stringify(params));
        if (params.students) {
            params.studentIds = params.students.map(item => item.id);
            delete params.students;
        }
        return await this.$axios.put(`/teacher/${this.route}/${id}`, params);
    },
    async delete(id, params) {
        return await this.$axios.delete(`/teacher/${this.route}/${id}`, params);
    },
};
