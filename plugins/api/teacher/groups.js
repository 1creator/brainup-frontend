export default {
    route: "groups",
    async fetch(params) {
        return await this.$axios.get(`/teacher/${this.route}`, { params });
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`/teacher/${this.route}/${id}`, { params });
    },
    async store(params) {
        params = JSON.parse(JSON.stringify(params));
        if (params.students) {
            params.studentIds = params.students.map(item => item.id);
            delete params.students;
        }
        return await this.$axios.post(`/teacher/${this.route}`, params);
    },
    async update(id, params) {
        if (params.students) {
            params.studentIds = params.students.map(item => item.id);
            delete params.students;
        }
        return await this.$axios.put(`/teacher/${this.route}/${id}`, params);
    },
    async delete(id, params) {
        return await this.$axios.delete(`/teacher/${this.route}/${id}`, params);
    },
};
