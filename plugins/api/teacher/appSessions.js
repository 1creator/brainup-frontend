export default {
    route: "app-sessions",
    async fetch(params) {
        return await this.$axios.get(`/teacher/${this.route}`, { params });
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`/teacher/${this.route}/${id}`, { params });
    },
    async restart(id, params) {
        return await this.$axios.put(`/teacher/${this.route}/${id}/restart`, { params });
    },
    async updateSettings(id, params) {
        return await this.$axios.put(`/teacher/${this.route}/${id}/settings`, params);
    },
    async answer(id, params) {
        return await this.$axios.post(`/teacher/${this.route}/${id}/answer`, params);
    },
    async getTask(id) {
        return await this.$axios.get(`/teacher/${this.route}/${id}/task`);
    },
};
