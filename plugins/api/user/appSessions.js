export default {
    route: "app-sessions",
    async fetch(params) {
        return await this.$axios.get(`/student/${this.route}`, { params });
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`/student/${this.route}/${id}`, { params });
    },
    async restart(id, params) {
        return await this.$axios.put(`/student/${this.route}/${id}/restart`, { params });
    },
    async updateSettings(id, params) {
        return await this.$axios.put(`/student/${this.route}/${id}/settings`, params);
    },
    async answer(id, params) {
        return await this.$axios.post(`/student/${this.route}/${id}/answer`, params);
    },
    async getTask(id) {
        return await this.$axios.get(`/student/${this.route}/${id}/task`);
    },
};
