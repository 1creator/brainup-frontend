export default {
    async fetch(params) {
        return await this.$axios.get("/student/leader-board", { params });
    },
};
