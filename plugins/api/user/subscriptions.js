export default {
    route: "subscriptions",
    async fetchTransactions(params) {
        // params = JSON.parse(JSON.stringify(params));
        return await this.$axios.get(`/student/${this.route}/transactions`, { params });
    },
    async buySubscription(params) {
        // params = JSON.parse(JSON.stringify(params));
        return await this.$axios.get(`/student/${this.route}/buy-subscription`, {
            params,
        });
    },
};
