export default {
    route: "invites",
    async fetch(params) {
        return await this.$axios.get(`/student/${this.route}`, { params });
    },
    async accept(id, params) {
        return await this.$axios.post(`/student/${this.route}/${id}/accept`, { params });
    },
    async decline(id, params) {
        return await this.$axios.delete(`/student/${this.route}/${id}`, params);
    },
};
