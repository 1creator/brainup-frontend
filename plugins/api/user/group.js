export default {
    route: "group",
    async fetch(params) {
        return await this.$axios.get(`/student/${this.route}`, { params });
    },
    async leave(params) {
        return await this.$axios.post(`/student/${this.route}/leave`, params);
    },
};
