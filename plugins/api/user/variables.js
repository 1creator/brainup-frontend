const route = "variables";

export default {
    async fetch(ids) {
        return await this.$axios.get(`admin/${route}`, { params: { ids } });
    },
};
