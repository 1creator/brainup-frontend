export default {
    route: "attestations",
    async fetch(params) {
        return await this.$axios.get(`/student/${this.route}`, { params });
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`/student/${this.route}/${id}`, { params });
    },
    async delete(id, params) {
        return await this.$axios.delete(`/student/${this.route}/${id}`, { params });
    },
    async store(params) {
        return await this.$axios.post(`/student/${this.route}`, params);
    },
    async fetchStage(stageId, params) {
        return await this.$axios.get(`/student/attestation-stages/${stageId}`, { params });
    },
    async answerStage(stageId, params) {
        return await this.$axios.post(`/student/attestation-stages/${stageId}/answer`, params);
    },
};
