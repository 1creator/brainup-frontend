export default {
    route: "payments",
    async getPaymentAccountType(id) {
        return await this.$axios.get(`/payments/${id}/account-type`);
    },
};
