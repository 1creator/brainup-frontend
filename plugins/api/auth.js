export default {
    async login(params) {
        return await this.$axios.post("/login", params);
    },
    async register(params) {
        return await this.$axios.post("/register", params);
    },
    async fetch(params) {
        return await this.$axios.get("/profile", { params });
    },
    async updateProfile(params) {
        params = JSON.parse(JSON.stringify(params));
        if (params.image !== undefined) {
            params.imageId = params.image && params.image.id;
            delete params.image;
        }
        return await this.$axios.patch("/profile", params);
    },
    async sendResetPasswordCode(params) {
        return await this.$axios.get("/reset-password", { params });
    },
    async resetPassword(params) {
        return await this.$axios.post("/reset-password", params);
    },
};
