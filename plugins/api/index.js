import qs from "qs";
import humps from "humps";

export default function({ $axios, redirect, store, $cookies, $config, error, req }, inject) {
    $axios.onRequest((config) => {
        if ("humps" in config && config.humps === false) {
            return config;
        }

        if (config.params) {
            config.params = humps.decamelizeKeys(config.params);

            config.paramsSerializer = config.arrayFormatComma = (params) => {
                return qs.stringify(params, {
                    arrayFormat: config.arrayFormat,
                });
            };
        }

        if (config.data) {
            config.data = humps.decamelizeKeys(config.data);
        }
    });

    $axios.onResponse((response) => {
        if (!("humps" in response.config) || response.config.humps === true) {
            return humps.camelizeKeys(response.data);
        } else {
            return response.data;
        }
    });

    $axios.onError((err) => {
        return Promise.reject(err.response);
    });

    // $axios.onError((err) => {
    // eslint-disable-next-line no-console
    // console.error(err.message, err.config);
    // switch (err.response.status) {
    // case 401:
    //     error({
    //         statusCode: 401,
    //         message: "Кажется, вы не авторизовались. Пожалуйста войдите в свою учетную запись.",
    //     });
    //     break;
    // case 403:
    //     error({
    //         statusCode: 403,
    //         message: "Недостаточно прав.",
    //     });
    //     break;
    // }
    // return error(err);
    // return Promise.reject(err.response);
    // });

    $axios.setBaseURL($config.apiHost);

    inject("api", {
        register(modules) {
            for (const key of Object.keys(modules)) {
                modules[key].$axios = $axios;
                this[key] = modules[key];
            }
        },
    });
}
