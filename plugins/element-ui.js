import Vue from "vue";
import { ColorPicker, DatePicker, TimePicker, Slider as ElSlider } from "element-ui";
import lang from "element-ui/lib/locale/lang/ru-RU";
import locale from "element-ui/lib/locale";

locale.use(lang);
export default () => {
    Vue.use(TimePicker, { locale });
    Vue.use(DatePicker, { locale });
    Vue.use(ColorPicker);
    Vue.use(ElSlider);
};
