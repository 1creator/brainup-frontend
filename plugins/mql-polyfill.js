const proto = Object.getPrototypeOf(matchMedia(""));
if (!proto.addEventListener) {
    proto.addEventListener = function(type, f) {
        this.addListener(f);
    };
}
