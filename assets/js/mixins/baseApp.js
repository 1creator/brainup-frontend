import ResizeObserver from "resize-observer-polyfill";

export default {
    props: {
        id: {
            type: [Number, String],
            required: true,
        },
        sidebar: {
            type: Boolean,
            default: true,
        },
        disableSound: {
            type: Boolean,
            default: false,
        },
        session: {
            type: Object,
            required: true,
        },
        hideProgress: {
            type: Boolean,
            default: false,
        },
    },
    data() {
        return {
            d_session: this.session,
            d_backgroundSound: this.$config.apiHost + "/sound/background/background.mp3",
            d_answerTrueSounds: [
                this.$config.apiHost + "/sound/result/здорово.mp3",
                this.$config.apiHost + "/sound/result/молодец.mp3",
                this.$config.apiHost + "/sound/result/неплохо.mp3",
                this.$config.apiHost + "/sound/result/удачно.mp3",
            ],
            d_answerFalseSounds: [
                this.$config.apiHost + "/sound/result/не_повезло.mp3",
                this.$config.apiHost + "/sound/result/неправильно.mp3",
                this.$config.apiHost + "/sound/result/ошибочка.mp3",
                this.$config.apiHost + "/sound/result/ты_ошибся.mp3",
            ],
            d_finishSounds: [this.$config.apiHost + "/sound/result/отличный_результат.mp3",
                this.$config.apiHost + "/sound/result/неплохой_результат.mp3",
                this.$config.apiHost + "/sound/result/попробуй_еще_раз.mp3",
            ],
            d_width: 0,
            d_height: 0,
            d_observer: null,
            d_showSettings: false,
            d_answering: false,
            d_task: null,
            d_result: null,
            d_taskLoading: false,
            d_answerLoading: false,
            d_answers: [],
        };
    },
    computed: {
        c_preferences() {
            return this.$store.state.preferences;
        },
        c_interfaceScale() {
            return {
                standard: 1,
                bigger: 1.25,
                biggest: 1.4,
            }[this.c_preferences.interfaceScale] || 1;
        },
        c_appStyle() {
            let baseFont;
            if (this.d_width < 1980 && this.d_width > 992) {
                baseFont = 20;
            }
            if (this.d_width > 576 && this.d_width < 992) {
                baseFont = 23;
            }
            if (this.d_width < 576) {
                baseFont = 26;
            }
            return {
                fontSize: this.c_interfaceScale * baseFont + "px",
            };
        },
        c_class() {
            const res = [];
            if (this.$store.state.media.xl) {
                res.push("app_desktop");
            } else if (this.$store.state.media.lg) {
                res.push("app_tablet");
            } else {
                res.push("app_mobile");
            }

            if (this.d_height > this.d_width) {
                res.push("app_portrait");
            }

            if (this.c_preferences.colorScheme !== "standard") {
                res.push("app_" + this.c_preferences.colorScheme);
            }

            if (this.c_preferences.pictures === false) {
                res.push("app_no-pictures");
            }

            if (this.c_preferences.interfaceScale !== "standard") {
                res.push("app_" + this.c_preferences.interfaceScale);
            }

            return res.join(" ");
        },
    },
    mounted() {
        this.initSounds(this.d_answerFalseSounds);
        this.initSounds(this.d_answerTrueSounds);
        this.initSounds(this.d_finishSounds);
        this.initBackgroundSound();
        this.playBackgroundSound();
        this.d_width = this.$el.parentElement.offsetWidth;
        this.d_height = this.$el.parentElement.offsetHeight;

        this.d_observer = new ResizeObserver((entries) => {
            this.d_width = this.$el.parentElement.offsetWidth;
            this.d_height = this.$el.parentElement.offsetHeight;
        });
        this.d_observer.observe(this.$el);
    },
    beforeDestroy() {
        this.d_backgroundSound.pause();
        this.d_observer.unobserve(this.$el);
    },
    watch: {
        "c_preferences.music"() {
            try {
                if (this.c_preferences.music) {
                    this.d_backgroundSound.play();
                } else {
                    this.d_backgroundSound.pause();
                }
            } catch (e) {
            }
        },
    },
    methods: {
        async getTask() {
            if (this.d_showSettings) {
                return;
            }
            this.d_taskLoading = true;
            this.d_task = await this.$api.appSessions.getTask(this.d_session.id);
            this.d_session.settings.watchTime = this.d_task.watchTime;
            this.d_taskLoading = false;
        },
        async onAnswer(answer) {
            this.d_answerLoading = true;
            this.d_result = await this.$api.appSessions.answer(this.session.id, {
                answer: answer.join(";"),
            });
            this.d_answerLoading = false;
            this.d_session.quality = this.d_result.quality;

            if (!this.d_result.nextTaskIndex) {
                await this.onFinish();
            } else {
                this.playAnswerSound(this.d_result.result);
                this.d_session.step = this.d_result.nextTaskIndex;
            }
        },
        showSettings() {
            this.d_showSettings = true;
            this.$emit("show-settings");
        },
        async onFinish() {
            this.playFinishSound(this.d_session.quality);
            this.d_session = await this.$api.appSessions.fetchOne(this.d_session.id);
        },
        initSounds(arr) {
            for (let i = 0; i < arr.length; i++) {
                arr[i] = new Audio(arr[i]);
            }
        },
        initBackgroundSound() {
            this.d_backgroundSound = new Audio(this.d_backgroundSound);
            this.d_backgroundSound.loop = true;
            this.d_backgroundSound.volume = 0.1;
        },
        playBackgroundSound() {
            if (this.c_preferences.music) {
                try {
                    this.d_backgroundSound.play();
                } catch (e) {

                }
            }
        },
        playAnswerSound(val) {
            if (this.c_preferences.sound) {
                try {
                    if (val) {
                        this.d_answerTrueSounds[Math.floor(Math.random() * this.d_answerTrueSounds.length)].play();
                    } else {
                        this.d_answerFalseSounds[Math.floor(Math.random() * this.d_answerFalseSounds.length)].play();
                    }
                } catch (e) {

                }
            }
        },
        playFinishSound(quality) {
            if (!this.disableSound) {
                try {
                    if (quality > 90) {
                        this.d_finishSounds[0].play();
                    } else if (quality > 70) {
                        this.d_finishSounds[1].play();
                    } else {
                        this.d_finishSounds[2].play();
                    }
                } catch (e) {

                }
            }
        },
        async restart() {
            this.cancelCurrentTask();
            this.d_session = await this.$api.appSessions.restart(this.d_session.id);
        },
        cancelCurrentTask() {
            this.d_task = null;
            this.d_answering = false;
        },
        async onTaskResultFinish() {
            this.d_task = this.d_result = this.d_answering = null;

            if (this.c_preferences.fastMode) {
                await this.getTask();
            }
        },
    },
};
