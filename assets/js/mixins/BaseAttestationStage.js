import commaFormat from "assets/js/filters/commaFormat";

export default {
    props: {
        printVariant: {
            type: Number,
            default: null,
        },
        stage: {
            type: Object,
            required: true,
        },
        showControls: {
            type: Boolean,
            default: true,
        },
        showAnswers: {
            type: Boolean,
            default: true,
        },
        withTimer: {
            type: Boolean,
            default: true,
        },
    },
    async fetch() {
        if (!this.stage.tasks) {
            try {
                this.d_stage = await this.$api.attestations.fetchStage(this.stage.id, {
                    start: 1,
                });
            } catch (e) {
                this.d_needSubscription = true;
            }
        }
        if (this.d_stage.finishedAt) {
            this.d_answers = this.d_stage.userAnswers;
        }
    },
    data() {
        return {
            d_stage: JSON.parse(JSON.stringify(this.stage)),
            d_answers: [],
            d_needSubscription: false,
        };
    },
    computed: {
        c_showAnswers() {
            return this.d_stage.answers && this.showAnswers;
        },
    },
    methods: {
        commaFormat,
        onTimeLeft() {
            if (this.$refs.footer) {
                this.$refs.footer.submit();
            }
        },
    },
};
