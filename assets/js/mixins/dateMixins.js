import moment from "moment";

moment.locale("ru");

export default {
    methods: {
        formatCalendar(val) {
            return moment(val).calendar();
        },
        formatDate(val) {
            return moment(val).format("L");
        },
        formatDateTime(val) {
            return moment(val).format("L");
        },
        formatFromNow(val) {
            return moment(val).fromNow();
        },
        isBefore(val) {
            return moment(val).isBefore();
        },
        isAfter(val) {
            return moment(val).isAfter();
        },
    },
};
