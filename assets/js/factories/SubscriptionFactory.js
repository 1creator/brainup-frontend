/**
 * @typedef {Object} Subscription
 * @property {number} id
 * @property {string} createdAt
 * @property {string} expiredAt
 */

export default {
    /**
     * @returns {Subscription}
     */
    create(props = {}) {
        return {
            ...{
                id: null,
                createdAt: null,
                expiredAt: null,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    },
};
