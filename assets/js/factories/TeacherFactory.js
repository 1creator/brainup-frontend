/**
 * @typedef {Object} Teacher
 * @property {number} id
 * @property {string} firstName
 * @property {string} lastName
 * @property {string} login
 * @property {string} email
 * @property {string} phone
 * @property {string} birthday
 */

export default {
    /**
     * @returns {Teacher}
     */
    create(props = {}) {
        return {
            ...{
                id: null,
                firstName: null,
                lastName: null,
                login: null,
                email: null,
                phone: null,
                birthday: null,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    },
};
