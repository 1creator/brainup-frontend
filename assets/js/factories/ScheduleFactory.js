/**
 * @typedef {Object} Group
 * @property {number} id
 * @property {string} name
 * @property {number} groupId
 * @property {Object} group
 * @property {string} date
 * @property {string} startAt
 * @property {string} finishAt
 * @property {boolean} mon
 * @property {boolean} tue
 * @property {boolean} wed
 * @property {boolean} thu
 * @property {boolean} fri
 * @property {boolean} sat
 * @property {boolean} sun
 */

export default {
    /**
     * @returns {Direction} direction
     */
    create(props = {}) {
        return {
            ...{
                id: null,
                groupId: null,
                group: null,
                date: null,
                startAt: null,
                finishAt: null,
                mon: false,
                tue: false,
                wed: false,
                thu: false,
                fri: false,
                sat: false,
                sun: false,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    },
};
