/**
 * @typedef {Object} MultiplayerPlayer
 * @property {number} id
 * @property {string} name
 * @property {Object} student
 * @property {Object} app
 * @property {Object} session
 * @property {Object[]} apps
 */

export default {
    /**
     * @returns {Student}
     */
    create(props = {}) {
        return {
            ...{
                id: Math.random(),
                student: null,
                name: null,
                app: null,
                session: null,
                apps: null,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    },
};
