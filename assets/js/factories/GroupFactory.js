/**
 * @typedef {Object} Group
 * @property {number} id
 * @property {string} description
 * @property {string} name
 */

export default {
    /**
     * @returns {Student}
     */
    create(props = {}) {
        return {
            ...{
                id: null,
                description: null,
                name: null,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    },
};
