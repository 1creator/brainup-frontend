/**
 * @typedef {Object} Student
 * @property {number} id
 * @property {string} firstName
 * @property {string} lastName
 * @property {string} middleName
 * @property {string} login
 * @property {string} email
 * @property {string} phone
 * @property {string} birthday
 */

export default {
    /**
     * @returns {Student}
     */
    create(props = {}) {
        return {
            ...{
                id: undefined,
                firstName: null,
                lastName: null,
                middleName: null,
                login: null,
                email: null,
                phone: null,
                birthday: null,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    },
};
