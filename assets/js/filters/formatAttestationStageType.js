export default function(type) {
    return {
        addition: "Сложение и вычитание",
        multiplication: "Умножение",
        division: "Деление",
    }[type] ?? type;
}
