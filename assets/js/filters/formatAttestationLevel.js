export default function(level) {
    switch (+level) {
    case 13:
        return "PRE-12";
    case 1:
        return "PRE-1";
    case 0:
        return 1;
    default:
        return level;
    }
}
