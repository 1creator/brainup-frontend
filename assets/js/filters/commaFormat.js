const nf = new Intl.NumberFormat("en-US");

export default function commaFormat(number) {
    return number ? nf.format(number) : "";
}
