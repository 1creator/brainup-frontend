export default function(type) {
    return {
        dynamic: "Динамический тренажер",
        static: "Статический тренажер",
        flash: "Флэш",
        multiplication: "Умножение",
    }[type] ?? type;
}
