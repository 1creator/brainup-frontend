export default function(type) {
    return {
        mental: "Ментальный счет",
        abakus: "Абакус",
    }[type] ?? type;
}
