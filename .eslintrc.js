module.exports = {
    root: true,
    env: {
        browser: true,
        node: true,
    },
    parserOptions: {
        ecmaVersion: 2020,
    },
    extends: ["@nuxtjs", "plugin:nuxt/recommended"],
    plugins: [], // add your custom rules here
    rules: {
        quotes: ["error", "single"],
        "object-curly-spacing": ["error", "always"],
        "comma-dangle": ["error", "always-multiline"],
        "space-before-function-paren": ["error", "never"],
        semi: ["error", "always"],
        indent: ["error", 4],
        eqeqeq: "off",
        "vue/html-indent": ["error", 4, {
            attribute: 1,
            baseIndent: 1,
            closeBracket: 0,
            alignAttributesVertically: true,
            ignores: [],
        }],
        "vue/no-v-html": "off",
        "vue/first-attribute-linebreak": ["error", {
            singleline: "beside",
            multiline: "beside",
        }],
        "vue/max-attributes-per-line": ["error", {
            singleline: {
                max: 1,
            },
            multiline: {
                max: 1,
            },
        }],
        "vue/multi-word-component-names": "off",
        "vue/no-mutating-props": "off",
    },
};
