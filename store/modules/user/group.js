export const state = () => {
    return {
        id: null,
        name: null,
        description: null,
        schedules: [],
        teacher: null,
        students: [],
    };
};

export const mutations = {
    setState(state, data) {
        Object.assign(state, data);
    },
};

export const getters = {};

export const actions = {
    async fetch({ commit }) {
        try {
            const res = await this.$api.group.fetch();
            if (res) {
                commit("setState", res);
            }
        } catch (e) {
            // пользователь не состоит в группе
        }
    },
    async leave(ctx, data) {
        await this.$api.group.leave();
        ctx.commit("setState", {
            id: null,
            name: null,
            description: null,
            schedules: [],
            students: [],
            teacher: null,
        });
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions,
};
