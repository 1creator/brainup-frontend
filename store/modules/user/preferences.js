export const state = () => {
    return {
        pictures: true,
        colorScheme: "standard",
        interfaceScale: "standard",
        beadImg: "light",
        fastMode: false,
        music: true,
        sound: true,
    };
};

const COOKIE_KEY = "preferences";

export const mutations = {
    update(state, data) {
        Object.assign(state, data);

        this.$cookies.set(COOKIE_KEY, state, {
            maxAge: 2147483647, // forever
            path: "/",
        });
    },
    restore(state) {
        const val = this.$cookies.get(COOKIE_KEY, { parseJSON: true });
        if (val && typeof val === "object") {
            Object.assign(state, val);
        }
    },
};

export const getters = {};

export const actions = {
    init({
        commit,
    }) {
        commit("restore");
    },
    update({ commit }, data) {
        commit("update", data);
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions,
};
