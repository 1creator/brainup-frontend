export const state = () => {
    return {
        subscriptionPackagePrice5: null,
        subscriptionPackagePrice30: null,
        subscriptionPackagePrice50: null,

        subscriptionTeacherPrice1: null,
        subscriptionTeacherPrice6: null,
        subscriptionTeacherPrice12: null,

        subscriptionStudentPrice1: null,
        subscriptionStudentPrice3: null,
        subscriptionStudentPrice6: null,
        subscriptionStudentPrice9: null,
        subscriptionStudentPrice12: null,

        attestationCertificateTemplate: null,

        notificationEmail: null,
    };
};

export const mutations = {
    updateState(state, data) {
        if (!state) {
            state = {};
        }
        Object.assign(state, data);
    },
};

export const getters = {};

export const actions = {
    async init({
        state,
        commit,
    }) {
        const ids = Object.keys(state);
        commit("updateState", await this.$api.variables.fetch({ ids }));
    },
    async update(context, data) {
        const res = await this.$api.variables.update(data);
        const resObj = res.reduce((acc, i) => {
            acc[i.id] = i.value;
            return acc;
        }, {});
        context.commit("updateState", resObj);
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions,
};
