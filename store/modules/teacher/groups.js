export const state = () => {
    return {
        items: [],
    };
};

export const mutations = {
    setState(state, data) {
        Object.assign(state, data);
    },
    add(state, item) {
        state.items.push(item);
    },
    update(state, { id, data }) {
        const updatingItem = state.items.find(item => item.id === +id);
        if (updatingItem) {
            Object.assign(updatingItem, data);
        }
    },
    remove(state, id) {
        const index = state.items.findIndex(item => item.id === +id);
        if (index >= 0) {
            state.items.splice(index, 1);
        }
    },
};

export const getters = {
    findById: state => (id) => {
        return state.items.find(item => item.id === +id);
    },
    defaultGroup: (state) => {
        return state.items.find(item => item.name === "Нераспределенные ученики");
    },
    // groupsWithoutEmptyDefault: state => {
    //     return state.items.filter(item => {
    //         return item.name === "Нераспределенные ученики";
    //     });
    // }
};

export const actions = {
    async fetch(ctx, data) {
        ctx.commit("setState", {
            items: await this.$api.groups.fetch(),
        });
    },
    async store(context, data) {
        const res = await this.$api.groups.store(data);
        context.commit("add", res);
        context.dispatch("students/syncGroup", {
            groupId: res.id,
            studentIds: res.students.map(item => item.id),
        }, { root: true });
    },
    async update(context, { id, data }) {
        const res = await this.$api.groups.update(id, data);
        context.commit("update", {
            id,
            data: res,
        });
        context.dispatch("students/syncGroup", {
            groupId: res.id,
            studentIds: res.students.map(item => item.id),
        }, { root: true });
    },
    async remove(context, { id }) {
        await this.$api.groups.delete(id);
        context.commit("remove", id);
        context.dispatch("students/syncGroup", {
            groupId: id,
            studentIds: [],
        }, { root: true });
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions,
};
