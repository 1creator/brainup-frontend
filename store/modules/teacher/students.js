export const state = () => {
    return {
        items: [],
    };
};

export const mutations = {
    setState(state, data) {
        Object.assign(state, data);
    },
    add(state, item) {
        state.items.push(item);
    },
    update(state, { id, data }) {
        const updatingItem = state.items.find(item => item.id === id);
        if (updatingItem) {
            Object.assign(updatingItem, data);
        }
    },
    remove(state, id) {
        const index = state.items.findIndex(item => item.id === id);
        if (index >= 0) {
            state.items.splice(index, 1);
        }
    },
};

export const getters = {
    findByGroupId: state => (id) => {
        return state.items.filter(item => item.groupId === +id);
    },
    findById: state => (id) => {
        return state.items.find(item => item.id === +id);
    },
};

export const actions = {
    async fetch(ctx, data) {
        ctx.commit("setState", {
            items: await this.$api.students.fetch(),
        });
    },
    async store(context, data) {
        const res = await this.$api.students.store(data);
        context.commit("add", res);
    },
    async update(context, { id, data }) {
        context.commit("update", {
            id,
            data: await this.$api.students.update(id, data),
        });
    },
    async giveSubscription(context, { id, data }) {
        const res = await this.$api.subscriptions.give({
            student_id: id,
        });

        context.commit("update", {
            id,
            data: {
                lastSubscription: res,
            },
        });

        context.commit("auth/updateUser", {
            subscriptionsLeft: context.rootState.auth.user.subscriptionsLeft - 1,
        }, {
            root: true,
        });
        return res;
    },
    async remove(context, { id }) {
        await this.$api.students.delete(id);
        context.commit("remove", id);
    },
    syncGroup({ state, commit, rootGetters }, { groupId, studentIds }) {
        state.items.forEach((item) => {
            if (studentIds.includes(item.id) && item.groupId !== groupId) {
                commit("update", {
                    id: item.id,
                    data: {
                        groupId,
                    },
                });
            } else if (!studentIds.includes(item.id) && item.groupId === groupId) {
                commit("update", {
                    id: item.id,
                    data: {
                        groupId: rootGetters["groups/defaultGroup"].id,
                    },
                });
            }
        });
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions,
};
