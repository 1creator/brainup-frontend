export const state = () => {
    return {
        user: null,
        token: null,
        accountType: "student",
    };
};

const TOKEN_KEYS = {
    admin: "adminToken",
    teacher: "teacherToken",
    student: "studentToken",
};

export const mutations = {
    updateState(state, data) {
        Object.assign(state, data);
        if ("token" in data) {
            this.$axios.setToken(data.token, "Bearer");
            if (data.token) {
                this.$cookies.set(TOKEN_KEYS[data.accountType], data.token, {
                    maxAge: 31536000, // 1 year
                    path: "/",
                });
            } else {
                this.$cookies.remove(TOKEN_KEYS[data.accountType]);
            }
        }
    },
};

export const getters = {
    isAuthenticated: state => state.token && state.user,
    home: (state) => {
        switch (state.accountType) {
        case "admin":
            return "/admin";
        case "teacher":
            return "/teacher";
        case "student":
        default:
            return "/student";
        }
    },
    buildUrl: state => url => `/${state.accountType}${url}`,
    // hasAttestationAccess: state => [
    //     "volkov@1creator.ru",
    //     "baltikova@yandex.ru",
    //     "spetrakova54@gmail.com",
    //     "ya.viktoriya-paliy@yandex.ru",
    //     "volk_234@mail.ru",
    // ]
    //     .includes(state.user?.email?.toLowerCase()),
    username: state => state.user.lastName + " " + state.user.firstName,
    imageUrl: state => state.user.image?.url,
    hasAccess: state => state.user.hasAccess,
};

export const actions = {
    async login({
        commit,
        dispatch,
        $axios,
    }, data) {
        const {
            user,
            apiToken,
        } = await this.$api.auth.login(data);

        commit("updateState", {
            user,
            token: apiToken,
            accountType: data.type,
        });

        switch (data.type) {
        case "admin":
            await dispatch("initAdmin", null, { root: true });
            break;
        case "teacher":
            await dispatch("initTeacher", null, { root: true });
            break;
        default:
        case "student":
            await dispatch("initStudent", null, { root: true });
            break;
        }
    },
    logout({ commit }, type) {
        commit("updateState", {
            token: null,
            user: null,
            accountType: type,
        });
    },
    async update(context, data) {
        const res = await this.$api.auth.updateProfile(data);
        context.commit("updateUser", res);
    },
    async register({
        dispatch,
        commit,
    }, data) {
        const {
            user,
            apiToken,
        } = await this.$api.auth.register(data);
        commit("updateState", {
            user,
            token: apiToken,
            accountType: data.type,
        });

        switch (data.type) {
        case "admin":
            await dispatch("initAdmin", null, { root: true });
            break;
        case "teacher":
            await dispatch("initTeacher", null, { root: true });
            break;
        default:
        case "student":
            await dispatch("initStudent", null, { root: true });
            break;
        }
    },
    async init({
        dispatch,
        commit,
        rootGetters,
    }, accountType) {
        let user;
        let token = this.$cookies.get(TOKEN_KEYS[accountType]) || this.$router.currentRoute.query.token;

        this.$axios.setToken(token, "Bearer");

        if (token) {
            try {
                user = await this.$api.auth.fetch({ type: accountType });
            } catch (e) {
                token = null;
                this.$axios.setToken(null, "Bearer");
            }
        }

        commit("updateState", {
            user,
            token,
            accountType,
        });
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions,
};
