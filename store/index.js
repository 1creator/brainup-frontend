export const state = () => ({});

export const mutations = {};

export const actions = {
    async nuxtServerInit({
        state,
        dispatch,
        commit,
        getters,
    }, ctx) {
        await dispatch("init", ctx);
    },
    async nuxtClientInit({ dispatch }, ctx) {
        await dispatch("init", ctx);
    },
    async init({ dispatch }, ctx) {
        await dispatch("media/init");
        this.$api.register({ auth: (await import("~/plugins/api/auth")).default });

        if (ctx.route.path.startsWith("/teacher")) {
            await dispatch("auth/init", "teacher");
            await dispatch("initTeacher", process.client);
        } else if (ctx.route.path.startsWith("/admin")) {
            await dispatch("auth/init", "admin");
            await dispatch("initAdmin", process.client);
        } else {
            await dispatch("auth/init", "student");
            await dispatch("initStudent", process.client);
        }
    },
    async initStudent({
        commit,
        dispatch,
    }, preserveState) {
        this.$api.register(await import("~/plugins/api/user/index"));
        loadStoreModules.apply(this, [await import("~/store/modules/user/index"), preserveState]);
        if (!preserveState) {
            await Promise.allSettled([
                dispatch("settings/init"),
                dispatch("preferences/init"),
                dispatch("group/fetch"),
            ]);
        }
    },
    async initAdmin({
        commit,
        dispatch,
        rootGetters,
    }, preserveState) {
        this.$api.register(await import("~/plugins/api/admin/index"));
        loadStoreModules.apply(this, [await import("~/store/modules/admin/index"), preserveState]);
        if (!preserveState && rootGetters["auth/isAuthenticated"]) {
            await Promise.allSettled([
                dispatch("settings/init"),
                dispatch("preferences/init"),
            ]);
        }
    },
    async initTeacher({
        commit,
        dispatch,
        rootGetters,
    }, preserveState) {
        this.$api.register(await import("~/plugins/api/teacher/index"));
        loadStoreModules.apply(this, [await import("~/store/modules/teacher/index"), preserveState]);
        if (!preserveState && rootGetters["auth/isAuthenticated"]) {
            await Promise.allSettled([
                dispatch("settings/init"),
                dispatch("preferences/init"),
                dispatch("groups/fetch"),
                dispatch("students/fetch"),
            ]);
        }
    },
};

function loadStoreModules(modules, preserveState) {
    for (const key of Object.keys(modules)) {
        if (this.hasModule(key)) {
            this.unregisterModule(key);
        }
        this.registerModule(key, modules[key], {
            preserveState,
        });
    }
}
