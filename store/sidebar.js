const state = () => ({
    show: null,
});

const getters = {};

const actions = {};

const mutations = {
    toggle(state, value) {
        state.show = value !== undefined ? value : !state.show;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
