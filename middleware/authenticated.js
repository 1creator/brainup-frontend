export default function({
    store,
    route,
    redirect,
}) {
    if (!store.getters["auth/isAuthenticated"]) {
        if (route.path.startsWith("/teacher")) {
            return redirect("/teacher/login");
        } else if (route.path.startsWith("/admin")) {
            return redirect("/admin/login");
        } else {
            return redirect("/student/login");
        }
    }
}
