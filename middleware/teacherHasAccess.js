export default function({
    store,
    redirect,
}) {
    if (!store.state.auth.user.hasAccess) {
        return redirect("/teacher/subscriptions");
    }
}
