export default {
    srcDir: __dirname,
    buildDir: ".nuxt/site",
    /*
    ** Headers of the page
    ** See https://nuxtjs.org/api/configuration-head
    */
    head: {
        title: "Brain UP - Развитие навыков быстрого счета",
        meta: [
            { charset: "utf-8" },
            {
                name: "viewport",
                content: "width=device-width, initial-scale=1",
            },
            {
                hid: "description",
                name: "description",
                content: process.env.npm_package_description || "",
            },
            {
                name: "msapplication-TileColor",
                content: "#da532c",
            },
            {
                name: "theme-color",
                content: "#ffffff",
            },
        ],
        link: [
            {
                rel: "icon",
                type: "image/x-icon",
                href: "/favicon.ico",
            },
            {
                rel: "preconnect",
                href: "href=\"https://fonts.googleapis.com",
            },
            {
                rel: "preconnect",
                href: "https://fonts.gstatic.com",
            },
            {
                rel: "stylesheet",
                href: "https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;600;700;800&display=swap",
            },
            {
                rel: "apple-touch-icon",
                sizes: "180x180",
                href: "/apple-touch-icon.png",
            },
            {
                rel: "icon",
                type: "image/png",
                sizes: "32x32",
                href: "/favicon-32x32.png",
            },
            {
                rel: "icon",
                type: "image/png",
                sizes: "16x16",
                href: "/favicon-16x16.png",
            },
            {
                rel: "manifest",
                href: "/site.webmanifest",
            },
            {
                rel: "mask-icon",
                href: "/safari-pinned-tab.svg",
                color: "#5bbad5",
            },
        ],
    },
    /*
    ** Global CSS
    */
    // Global CSS (https://go.nuxtjs.dev/config-css)
    css: ["./assets/sass/vendor.scss", "./assets/sass/app.scss"],
    /*
    ** Plugins to load before mounting the App
    ** https://nuxtjs.org/guide/plugins
    */
    plugins: [
        "./plugins/api/index.js",
        "./plugins/element-ui.js",
        {
            src: "./plugins/nuxt-client-init.js",
            ssr: false,
        },
    ],
    /*
    ** Auto import components
    ** See https://nuxtjs.org/api/configuration-components
    */
    components: false,
    // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
    buildModules: [
        // https://go.nuxtjs.dev/eslint
        "@nuxtjs/eslint-module",
        "@nuxtjs/style-resources",
    ],
    /*
    ** Nuxt.js modules
    */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        "cookie-universal-nuxt",
        "@nuxtjs/pwa",
        "@nuxtjs/axios",
        "@nuxtjs/yandex-metrika",
    ],
    styleResources: {
        scss: [
            "~assets/sass/_variables.scss",
        ],
    },
    yandexMetrika: {
        id: "87447181",
        webvisor: true,
        clickmap: true,
        // useCDN:false,
        trackLinks: true,
        accurateTrackBounce: true,
    },
    /*
    ** Axios module configuration
    ** See https://axios.nuxtjs.org/options
    */
    axios: {},
    /*
    ** Build configuration
    ** See https://nuxtjs.org/api/configuration-build/
    */
    // build: {
    //     // vendor: ["axios"],
    //     extend(config, ctx) {
    //         const webpackConfig = require("../../webpack.config");
    //         Object.assign(config.resolve.alias, webpackConfig.resolve.alias);
    //     },
    // },

    router: {
        routeNameSplitter: ".",
    },

    publicRuntimeConfig: {
        apiHost: process.env.apiHost || "http://localhost:8000",
    },

    server: {
        host: "0.0.0.0",
    },
};
