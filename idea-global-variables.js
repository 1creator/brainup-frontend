/* eslint-disable no-undef */
// noinspection ES6UnusedImports,TypeScriptUMDGlobal

import * as adminApi from "./plugins/api/admin/index";
import * as studentApi from "./plugins/api/user/index";
import * as teacherApi from "./plugins/api/teacher/index";

Vue.prototype.$api = adminApi;
Vue.prototype.$api = studentApi;
Vue.prototype.$api = teacherApi;
